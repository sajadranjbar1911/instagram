<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Instagram</title>
    <link rel="stylesheet" href="{{asset('css/app.css')}}">
    <style>
        .rounded-form {
            border-radius: 1.3rem !important;
        }
    </style>
</head>
<body>
<div class="row justify-content-center">
    <div class="col-5 justify-content-center mt-5">
        <form method="post" action="{{ route('SignUp') }}" class="border rounded-form bg-white">
            @csrf
            <div class="form-row  m-auto">
                <h1 class="mx-auto mb-3 justify-content-center " style="font-family:cursive;">
                    Instagram
                </h1>
            </div>
            <div class="form-row  m-auto">
                <h2 class="mx-auto mb-3 justify-content-center text-muted form-text"
                    style="font-family:'monospace'; font-size: medium;">
                    Sign up to see photos and videos from your friends.
                </h2>
            </div>
            <div class="form-row  m-auto">
                <input type="text" name="fullName" class="form-control col-6 mx-auto my-1 justify-content-center"
                       placeholder="Full Name">
            </div>
            <div class="form-row  m-auto">
                <input type="email" name="email" class="form-control col-6 mx-auto my-1 justify-content-center"
                       placeholder="Email">
            </div>
            <div class="form-row  m-auto">
                <input type="tel" name="phone" class="form-control col-6 mx-auto my-1 justify-content-center"
                       placeholder="Phone Number">
            </div>
            <div class="form-row  m-auto">
                <input type="password" name="password" class="form-control col-6 mx-auto my-1 justify-content-center"
                       placeholder="Password">
            </div>
            <div class="form-row  m-auto col-7">
                <label class="sr-only" for="inlineFormInputGroup">Username</label>
                <div class="input-group">
                    <div class="input-group-prepend">
                        <div class="input-group-text">@</div>
                    </div>
                    <input type="text" name="username" class="form-control " id="inlineFormInputGroup" placeholder="Username">
                </div>
            </div>
            <div class="form-row  m-3 justify-content-center">
                <button class="btn btn-primary" type="submit">Sign Up</button>
            </div>
        </form>
    </div>
</div>
{{--<div class="row">--}}
{{--    <div class="col-6">--}}
{{--        ,--}}
{{--        1.2--}}
{{--    </div>--}}
{{--    <div class="col-6">--}}
{{--        <form class="justify-content-center ">--}}
{{--            <div class="form-row">--}}
{{--                <div class="col-5">--}}
{{--                    <input type="text" class="form-control " placeholder="Full Name">--}}
{{--                </div>--}}
{{--            </div>--}}
{{--            <div class="form-row">--}}
{{--                <div class="col-5">--}}
{{--                    <input type="text" class="form-control" placeholder="Username">--}}
{{--                </div>--}}
{{--            </div>--}}
{{--            <div class="form-row">--}}
{{--                <div class="col-5">--}}
{{--                    <input type="email" class="form-control" placeholder="Email">--}}
{{--                </div>--}}
{{--            </div>--}}
{{--            <div class="form-row">--}}
{{--                <div class="col-5">--}}
{{--                    <input type="password" class="form-control" placeholder="Password">--}}
{{--                </div>--}}
{{--            </div>--}}
{{--            <div class="form-row">--}}
{{--                <div class="col-5">--}}
{{--                    <input type="tel" class="form-control" placeholder="Phone Number">--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </form>--}}
{{--    </div>--}}
{{--</div>--}}
</body>
</html>
