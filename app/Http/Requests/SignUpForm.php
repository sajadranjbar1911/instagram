<?php

namespace App\Http\Requests;

use App\Rules\NotContaining;
use Illuminate\Foundation\Http\FormRequest;

class SignUpForm extends FormRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $input = [
            'fullName' => $this->input('fullName'),
            'email' => $this->input('email'),
            'phone' => $this->input('phone'),
            'username' => $this->input('username')

        ];
        return [
            'fullName' => ['required', 'alpha', 'min:5', 'max:50'],
            'email' => ['required', 'E-Mail'],
            'phone' => ['required', 'size:11', 'numeric'],
            'username' => ['required', 'min:5', 'max:15', 'alpha Dash'],
            'password' => ['required', 'min:6', 'max:15', new NotContaining($input)]
        ];
    }
}
